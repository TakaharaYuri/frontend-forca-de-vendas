import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Global } from 'src/app/@core/global';
import { EvoService } from 'src/app/@core/evo.service';
import { toInteger } from '@ng-bootstrap/ng-bootstrap/util/util';

declare interface RouteInfo {
    path: string;
    title: string;
    icon: string;
    class: string;
    nivel: string;
}
export const ROUTES: RouteInfo[] = [
    // { path: '/dashboard', title: 'Dashboard',  icon: 'ni-chart-bar-32 text-blue', class: '' },
    { path: '/gerencial', title: 'Configurações',  icon: 'ni-ui-04 text-blue', class: '', nivel:'manager' },
    { path: '/marketing', title: 'Marketing',  icon: 'ni-notification-70 text-blue', class: '', nivel:'manager' },
    { path: '/estoque', title: 'Estoque',  icon: 'ni-app text-blue', class: '', nivel:'manager' },
    { path: '/estoque', title: 'Estoque',  icon: 'ni-app text-blue', class: '', nivel:'manager' },
    { path: '/estoque', title: 'Estoque',  icon: 'ni-app text-blue', class: '', nivel:'manager' },
    { path: '/estoque', title: 'Estoque',  icon: 'ni-app text-blue', class: '', nivel:'manager' },
    { path: '/estoque', title: 'Estoque',  icon: 'ni-app text-blue', class: '', nivel:'manager' },
    { path: '/estoque', title: 'Estoque',  icon: 'ni-app text-blue', class: '', nivel:'manager' },
    { path: '/estoque', title: 'Estoque',  icon: 'ni-app text-blue', class: '', nivel:'manager' },
    { path: '/estoque', title: 'Estoque',  icon: 'ni-app text-blue', class: '', nivel:'manager' },
    // { path: '/gerencial/empresas', title: 'Empresas',  icon: 'ni-building text-orange', class: '' },
    // { path: '/gerencial/setores', title: 'Setores',  icon: 'ni-circle-08 text-green', class: '' },
    // { path: '/gerencial/usuarios', title: 'Usuários',  icon: 'ni-single-02 text-yellow', class: '' },
    { path: '/gerencial/configuracoes', title: 'Sistema',  icon: 'ni-settings-gear-65 text-danger', class: '', nivel:'admin' },
];

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss'],
  providers:[EvoService]
})
export class SidebarComponent implements OnInit {

  public menuItems: any[];
  public isCollapsed = true;
  public empresa;
  public empresa_padrao;
  public user;
  public empresaId = 1;
  public modules;

  constructor(
    private router: Router,
    public global: Global,
    public service: EvoService
    ) { }

  ngOnInit() {
    this.empresaId = Number(localStorage.getItem('empresa_id'));
    this.menuItems = ROUTES.filter(menuItem => menuItem);
    this.modules = this.global.getPermissions();
    this.router.events.subscribe((event) => {
      this.isCollapsed = true;
   });

    this.user = this.global.getLoggedUser();
    this.empresa = this.global.getLoggedUser().empresa_padrao; 
    this.empresa_padrao = this.global.getLoggedUser().empresas
      .find( 
        emp => emp.id == this.global.getLoggedUser().empresa_id
      );
  }

  changeEmpresaPadrao(item) {
    const newData = this.global.getLoggedUser();
    newData.empresa_padrao = item;
    newData.empresa = item;
    localStorage.setItem('user', JSON.stringify(newData));
    localStorage.setItem('empresa_id', item.id);
    window.location.reload();
  }

  showMenu(item) {
    item.show = (!item.show)? true: false;
  }
}
