import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ImageCroppedEvent } from 'ngx-image-cropper';
import { EvoService } from 'src/app/@core/evo.service';
import { Global } from 'src/app/@core/global';
import { catchError, map } from 'rxjs/operators';
import { of } from 'rxjs';
import { HttpEventType, HttpErrorResponse } from '@angular/common/http';
import { BlockUI, NgBlockUI } from 'ng-block-ui';

@Component({
  selector: 'app-modal-gallery',
  templateUrl: './modal-gallery.component.html',
  styleUrls: ['./modal-gallery.component.scss']
})
export class ModalGalleryComponent implements OnInit {
  @Input() config: any = {};
  @Output() event: EventEmitter<any> = new EventEmitter<any>();
  @BlockUI() blockUI: NgBlockUI;
  public uploadStep = 1;
  public aspectRatio: Number;
  public maintainAspectRatio = true;
  public imageChangedEvent: any = '';
  public croppedImage: any = '';
  public progress;
  public loading;
  public galleryFiles;
  public activeTab = 1;
  public selectedItems = [];
  public selectCount = 2;
  public files;
  public resizeToWidth;
  public resizeToHeight;
  public requiredSize = false;
  public resizeError = {
    result: false,
    message: ''
  }


  constructor(
    public global: Global,
    public activeModal: NgbActiveModal,
    private service: EvoService,
  ) {

  }

  close() {
    this.activeModal.close(false);
  }

  ngOnInit(): void {
    console.log('config', this.config);
    this.aspectRatio = this.config.aspectRatio;
    this.maintainAspectRatio = this.config.maintainAspectRatio;
    this.files = this.config.files;
    this.selectCount = this.config.selectCount;
    this.resizeToHeight = this.config.resizeToHeight;
    this.resizeToWidth = this.config.resizeToWidth;
    this.requiredSize = this.config.requiredSize;
    this.getGalleryData();
    // this.blockUI.start('Bom dia');
  }



  fileChangeEvent(event: any): void {
    this.imageChangedEvent = event;
    this.uploadStep = 2;
    this.resizeError.result = false;
  }
  imageCropped(event: ImageCroppedEvent) {
    this.croppedImage = event.base64;
    console.log('croped', event);
    if (this.requiredSize) {
      if (this.resizeToHeight && this.resizeToHeight) {
        if (event.height < this.resizeToHeight || event.width < this.resizeToWidth) {
          this.resizeError.result = true;
          this.resizeError.message = `O arquivo que você está tentando utilizar tem <b>${event.height}px</b> por <b>${event.width}px</b> as dimensões minimas para este arquivo são de: <b>${this.resizeToHeight}px</b> por <b>${this.resizeToWidth}px</b> `;
        }
      }
    }

  }
  imageLoaded() {
    // show cropper
  }
  cropperReady() {
    // cropper ready
  }
  loadImageFailed() {
    // show message
  }
  save() {
    const newFile = this.dataURItoBlob(this.croppedImage);
    this.upload(newFile);
  }

  upload(file) {
    this.blockUI.start('Enviando arquivo...');
    const formData = new FormData();
    formData.append('image', file);
    this.service.upload(formData).pipe(
      map(event => {
        switch (event.type) {
          case HttpEventType.UploadProgress:
            file.progress = Math.round(event.loaded * 100 / event.total);
            this.progress = file.progress;
            break;
          case HttpEventType.Response:
            return event;
        }
      }),
      catchError((error: HttpErrorResponse) => {
        file.inProgress = false;
        return of(`${file.name} upload failed.`);
      })).subscribe((event: any) => {
        if (typeof (event) === 'object') {
          this.event.emit(event.body.fileName);
          let _fileData = {
            url: event.body.fileName,
            meta_dados: event.body.data,
            nome: event.body.data.original_filename,
            tamanho: event.body.data.bytes,
            altura: event.body.data.height,
            largura: event.body.data.width
          }
          this.saveDataToGallery(_fileData);
        }
      });
  }

  dataURItoBlob(dataURI) {
    const type = 'image/png';
    const byteString = atob(dataURI.split(',')[1]);
    // const mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0]
    const ab = new ArrayBuffer(byteString.length);
    const ia = new Uint8Array(ab);
    for (let i = 0; i < byteString.length; i++) {
      ia[i] = byteString.charCodeAt(i);
    }
    const bb = new Blob([ab], { type: type });
    return bb;
  }

  saveDataToGallery(image) {
    this.loading = true;
    this.service.entityName = 'galeria-de-arquivos';
    this.service.createResource(image).subscribe(response => {
      this.blockUI.stop();
      this.global.iziToas.success({
        title: 'Pronto',
        message: 'Arquivo enviado com sucesso'
      });
      this.getGalleryData();

      this.activeTab = 1;
      this.imageChangedEvent = null;
      this.croppedImage = null;
      this.uploadStep = 1;
    });
  }

  getGalleryData() {
    if (this.files && this.files != null && this.files.length > 0) {
      this.service.entityName = 'galeria-de-arquivos/selected';
      this.service.createResource({ files: this.files }).subscribe((response: any) => {
        console.log('Response => ', response);
        this.galleryFiles = response.data;
        this.selectedItems = response.selectedItems;
      })
    }
    else {
      this.service.entityName = 'galeria-de-arquivos';
      this.service.getResources().subscribe(response => {
        this.galleryFiles = response;
        // this.selectedItems = [];
      })
    }
  }

  onSelectItems(item) {
    if (!item.selected) {
      if (this.requiredSize) {
        if (item.altura < this.resizeToHeight || item.largura < this.resizeToWidth) {
          this.global.iziToas.error({
            title: `O arquivo que você selecionou não tem as dimenções minimas requiridas ${this.resizeToHeight}px / ${this.resizeToWidth}px`
          })
        }
        else {
          if (this.selectedItems.length <= this.selectCount - 1) {
            item.selected = true;
            this.selectedItems.push(item);

          }
          else {
            this.global.iziToas.warning({
              title: `Você só pode selecionar ${this.selectCount} item${((this.selectCount - 1) > 1) ? 's' : ''}`
            })
          }
        }
      }
      else {
        if (this.selectedItems.length <= this.selectCount - 1) {
          item.selected = true;
          this.selectedItems.push(item);

        }
        else {
          this.global.iziToas.warning({
            title: `Você só pode selecionar ${this.selectCount} item${((this.selectCount - 1) > 1) ? 's' : ''}`
          })
        }
      }
    }
    else {
      item.selected = false;
      const index = this.selectedItems.indexOf(item);
      this.selectedItems.splice(index, 1);
    }
  }

  onInsertMedias() {
    console.log('onInsertMedias ->', this.selectedItems);
    let items: any;
    if (this.selectedItems.length == 1) {
      items = this.selectedItems[0];
    }
    else {
      items = this.selectedItems;
    }
    this.activeModal.close(items);
  }
}

