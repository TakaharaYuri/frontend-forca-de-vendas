import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Global } from 'src/app/@core/global';
import { ModalGalleryComponent } from './modal-gallery/modal-gallery.component';

@Component({
  selector: 'app-files-gallery',
  templateUrl: './files-gallery.component.html',
  styleUrls: ['./files-gallery.component.css']
})
export class FilesGalleryComponent implements OnInit {

  @Output() event: EventEmitter<any> = new EventEmitter<any>();
  @Input() type = 'image';
  @Input() aspectRatio = '2';
  @Input() maintainAspectRatio = true;
  @Input() btnText = 'Galeria de Imagens';
  @Input() icon = 'fa fa-file';
  @Input() customClass = '';
  @Input() files;
  @Input() selectCount = 2;
  @Input() resizeToWidth = 0;
  @Input() resizeToHeight = 0;
  @Input() disabled = false;
  @Input() requiredSize = false;

  constructor(
    public global: Global,
    private modalService: NgbModal
  ) { }

  ngOnInit(): void {
  }

  close() {

  }

  openDialog() {
    const modal = this.modalService.open(ModalGalleryComponent, {
      size: 'lg',
      keyboard: false,
      backdrop: 'static',
      windowClass: 'custom-modal-lg',
    });

    modal.componentInstance.config = {
      type: this.type,
      aspectRatio: this.aspectRatio,
      maintainAspectRatio: Number(this.maintainAspectRatio),
      files: this.files,
      selectCount: this.selectCount,
      resizeToWidth: this.resizeToWidth,
      resizeToHeight: this.resizeToHeight,
      requiredSize: this.requiredSize

    };

    modal.result.then((result) => {
      if (result) {
        return this.event.emit(result);
      }
    });
  }
}
