import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SidebarComponent } from './sidebar/sidebar.component';
import { NavbarComponent } from './navbar/navbar.component';
import { FooterComponent } from './footer/footer.component';
import { RouterModule } from '@angular/router';
import { NgbModule, NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';
import { PipesModule } from '../@core/pipes/pipes.module';
import { NgSelectModule } from '@ng-select/ng-select';
import { FormsModule } from '@angular/forms';
import { CardModuleComponent } from './generic/card-module/card-module.component';
import { PageHeaderComponent } from './generic/page-header/page-header.component';
import { FabButtonComponent } from './generic/fab-button/fab-button.component';
import { EmptyDataComponent } from './generic/empty-data/empty-data.component';
import { CardDashboardComponent } from './generic/card-dashboard/card-dashboard.component';
import { ButtonUploadComponent } from './button-upload/button-upload.component';
import { FormFieldRowComponent } from './form-field-row/form-field-row.component';
import { FilesGalleryComponent } from './files-gallery/files-gallery.component';
import { ModalGalleryComponent } from './files-gallery/modal-gallery/modal-gallery.component';
import { ImageCropperModule } from 'ngx-image-cropper';
import { BlockUIModule } from 'ng-block-ui';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';
const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true
};




@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    NgbModule,
    PipesModule,
    NgSelectModule,
    NgbTooltipModule,
    ImageCropperModule,
    BlockUIModule.forRoot(),
    PerfectScrollbarModule

  ],
  declarations: [
    FooterComponent,
    NavbarComponent,
    SidebarComponent,
    CardModuleComponent,
    PageHeaderComponent,
    FabButtonComponent,
    EmptyDataComponent,
    CardDashboardComponent,
    ButtonUploadComponent,
    FormFieldRowComponent,
    FilesGalleryComponent,
    ModalGalleryComponent,
  ],
  exports: [
    FooterComponent,
    NavbarComponent,
    SidebarComponent,
    CardModuleComponent,
    PageHeaderComponent,
    FabButtonComponent,
    EmptyDataComponent,
    CardDashboardComponent,
    ButtonUploadComponent,
    FormFieldRowComponent,
    FilesGalleryComponent,
  ],
  providers: [
    {
      provide: PERFECT_SCROLLBAR_CONFIG,
      useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
    }
  ]
})
export class ComponentsModule { }
