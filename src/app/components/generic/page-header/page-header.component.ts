import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-page-header',
  templateUrl: './page-header.component.html',
  styleUrls: ['./page-header.component.css']
})
export class PageHeaderComponent implements OnInit {

  @Input() image = 'assets/img/theme/background.jpeg';
  @Input() name = 'Módulo';
  @Input() description = '';
  @Input() showButtom = false;
  @Input() buttomRouter = '';
  @Input() buttomText;
  constructor() { }

  ngOnInit(): void {
  }

}
