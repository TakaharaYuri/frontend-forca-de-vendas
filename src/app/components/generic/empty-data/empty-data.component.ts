import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-empty-data',
  templateUrl: './empty-data.component.html',
  styleUrls: ['./empty-data.component.css']
})
export class EmptyDataComponent implements OnInit {
  @Input() text = 'Nenhum dado cadastrado';
  @Input() description = 'Adicione um novo registro agora mesmo e continue aproveitando o melhor do FDV';
  @Input() image = 'https://image.flaticon.com/icons/svg/2491/2491800.svg';
  constructor() { }

  ngOnInit(): void {
  }

}
