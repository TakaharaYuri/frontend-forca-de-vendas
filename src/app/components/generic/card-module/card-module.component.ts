import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-card-module',
  templateUrl: './card-module.component.html',
  styleUrls: ['./card-module.component.css']
})
export class CardModuleComponent implements OnInit {
  @Input() route = ['dashboard'];
  @Input() icon = 'fas fa-building';
  @Input() name = 'Módulo';
  @Input() text = 'Configurar';
  @Input() class = 'bg-primary';
  @Input() btnClass = 'btn-primary';
  @Input() disabled = false;
  

  constructor() { }

  ngOnInit(): void {
  }

}
