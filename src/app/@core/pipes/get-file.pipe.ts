import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer, SafeHtml, SafeStyle, SafeScript, SafeUrl, SafeResourceUrl } from '@angular/platform-browser';
import { environment } from 'src/environments/environment';

@Pipe({
  name: 'getFile'
})
export class GetFilePipe implements PipeTransform {
  public api
  constructor(protected sanitizer: DomSanitizer) {
    this.api = environment.apiUrl;
  }

  transform(value: unknown, ...args: unknown[]): unknown {
    if (value) {
      return this.sanitizer.bypassSecurityTrustResourceUrl(`${value}?token=${Date.now()}`);
    }
  }

}
