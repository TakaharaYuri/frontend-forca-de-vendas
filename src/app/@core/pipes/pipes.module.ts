import { NgModule } from '@angular/core';
import { SafePipe } from './safe.pipe';
import { LooseCurrencyPipe } from './currency.pipe';
import { CurrencyPipe } from '@angular/common';
import { GetFilePipe } from './get-file.pipe';
import { TruncatePipe } from './truncate.pipe';

@NgModule({
  declarations: [
    SafePipe,
    GetFilePipe,
    TruncatePipe
  ],
  imports: [

  ],
  exports: [
    SafePipe,
    GetFilePipe,
    TruncatePipe
  ],
  providers:[
    CurrencyPipe
  ]
})
export class PipesModule { }
