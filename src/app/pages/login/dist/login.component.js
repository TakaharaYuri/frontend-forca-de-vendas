"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.LoginComponent = void 0;
var core_1 = require("@angular/core");
var environment_1 = require("src/environments/environment");
var global_1 = require("src/app/@core/global");
var LoginComponent = /** @class */ (function () {
    function LoginComponent(http, global, route) {
        this.http = http;
        this.global = global;
        this.route = route;
        this.loading = false;
        this.host = environment_1.environment.apiUrl;
    }
    LoginComponent.prototype.ngOnInit = function () {
    };
    LoginComponent.prototype.ngOnDestroy = function () {
    };
    LoginComponent.prototype.login = function () {
        var _this = this;
        this.loading = true;
        var data = {
            email: this.email,
            password: this.password
        };
        this.http.post(this.host + "/login", data).subscribe((function (response) {
            _this.loading = false;
            if (response.result) {
                _this.global.iziToas.success({
                    title: 'Pronto!',
                    message: 'Login realizado com sucesso'
                });
                _this.route.navigate(['/gerencial']);
                localStorage.setItem('token', response.token.token);
                localStorage.setItem('user', JSON.stringify(response));
                localStorage.setItem('empresa', JSON.stringify(response.empresa_padrao));
                localStorage.setItem('empresa_id', JSON.stringify(response.empresa_padrao.id));
                localStorage.setItem('permissoes', JSON.stringify(response.permissoes));
                console.log('Response ->', response);
            }
            else {
                _this.global.iziToas.warning({
                    title: 'Ops!',
                    message: response.msg
                });
            }
        }), (function (error) {
            _this.loading = false;
            _this.global.iziToas.error({
                title: 'Ops',
                message: 'Email ou senha inválidos, verifique e tente novamente.',
                maxWidth: '300px'
            });
        }));
    };
    ;
    LoginComponent = __decorate([
        core_1.Component({
            selector: 'app-login',
            templateUrl: './login.component.html',
            styleUrls: ['./login.component.scss'],
            providers: [global_1.Global]
        })
    ], LoginComponent);
    return LoginComponent;
}());
exports.LoginComponent = LoginComponent;
