import { NgModule } from '@angular/core';
import { CommonModule, } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';

import { AdminLayoutComponent } from './layouts/admin-layout/admin-layout.component';
import { AuthLayoutComponent } from './layouts/auth-layout/auth-layout.component';
import { AuthGuardService } from './guards/auth-guard.service';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full',
  }, 
  {
    path: '',
    component: AdminLayoutComponent,
    canActivate: [AuthGuardService],
    children: [
      {
        path: '',
        loadChildren: () => import('./layouts/admin-layout/admin-layout.module').then(m => m.AdminLayoutModule)
      },
      {
        path: 'gerencial',
        loadChildren: () => import('./modules/gerencial/gerencial.module').then(m => m.GerencialModule)
      },
      {
        path: 'marketing',
        loadChildren: () => import('./modules/marketing/marketing.module').then(m => m.MarketingModule)
      },
      {
        path: 'estoque',
        loadChildren: () => import('./modules/estoque/estoque.module').then(m => m.EstoqueModule)
      },
      {
        path: 'administrador',
        loadChildren: () => import('./modules/administrador/administrador.module').then(m => m.AdministradorModule)
      },
      {
        path: 'showroom',
        loadChildren: () => import('./modules/showroom/showroom.module').then(m => m.ShowroomModule)
      },
      {
        path: 'seminovos',
        loadChildren: () => import('./modules/seminovos/seminovos.module').then(m => m.SeminovosModule)
      },
    ]
  }, 
  {
    path: '',
    component: AuthLayoutComponent,
    children: [
      {
        path: '',
        loadChildren: () => import('./layouts/auth-layout/auth-layout.module').then(m => m.AuthLayoutModule)
      }
    ]
  },
  {
    path: '**',
    redirectTo: 'gerencial'
  }
];

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    RouterModule.forRoot(routes, {
      useHash: true
    })
  ],
  exports: [
  ],
})
export class AppRoutingModule { }
