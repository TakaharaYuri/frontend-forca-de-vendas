import { Routes } from '@angular/router';

import { LoginComponent } from '../../pages/login/login.component';
import { RegisterComponent } from '../../pages/register/register.component';
import { RegistroComponent } from 'src/app/modules/autenticacao/registro/registro.component';
import { RegistroCadastroComponent } from 'src/app/modules/autenticacao/registro/registro-cadastro/registro-cadastro.component';
import { RegistroPerfilComponent } from 'src/app/modules/autenticacao/registro/registro-perfil/registro-perfil.component';
import { GeradorImagensInstagramComponent } from 'src/app/modules/autenticacao/gerador-imagens-instagram/gerador-imagens-instagram.component';
import { GerarImagemComponent } from 'src/app/modules/autenticacao/gerador-imagens-instagram/gerar-imagem/gerar-imagem.component';

export const AuthLayoutRoutes: Routes = [
    { path: 'login', component: LoginComponent },
    { path: 'cadastro', component: RegistroComponent },
    { path: 'cadastro/usuario', component: RegistroCadastroComponent },
    { path: 'cadastro/perfil', component: RegistroPerfilComponent },
    { path: 'gerador-instagram', component: GeradorImagensInstagramComponent },
    { path: 'gerador-instagram/gerar/:id', component: GerarImagemComponent },
    // { path: 'register/finalizar', component: RegisterComponent }
];
