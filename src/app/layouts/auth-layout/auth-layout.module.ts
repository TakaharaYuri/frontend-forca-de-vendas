import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { AuthLayoutRoutes } from './auth-layout.routing';

import { LoginComponent } from '../../pages/login/login.component';
import { RegisterComponent } from '../../pages/register/register.component';
import { AutenticacaoModule } from 'src/app/modules/autenticacao/autenticacao.module';
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AuthLayoutRoutes),
    FormsModule,
    AutenticacaoModule
    // NgbModule
  ],
  declarations: [
    LoginComponent,
    // RegisterComponent
  ]
})
export class AuthLayoutModule { }
