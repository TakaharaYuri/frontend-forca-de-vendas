import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { EvoService } from 'src/app/@core/evo.service';
import { Global } from 'src/app/@core/global';
import { ModalModelosComponent } from './modal-modelos/modal-modelos.component';

@Component({
  selector: 'app-modelos',
  templateUrl: './modelos.component.html',
  styleUrls: ['./modelos.component.css']
})
export class ModelosComponent implements OnInit {

  public data;

  constructor(
    public global: Global,
    public service: EvoService,
    private modalService: NgbModal
  ) {
    this.service.entityName = 'gerencial/modelos';
  }

  ngOnInit(): void {
    this.get();
  }

  open(data = {}) {
    const modal = this.modalService.open(ModalModelosComponent, {
      size: 'lg',
      keyboard: false,
      backdrop: 'static'
    });

    modal.componentInstance.entity = data;

    modal.result.then((result) => {
      if (result) {
        this.get();
      }
    });
  }

  get() {
    this.service.getResources().subscribe(response => {
      this.data = response;
      console.log(response);
    })
  }

  updateStatus(item) {
    if (item.status == 1) {
      item.status = 0;
    }
    else {
      item.status = 1;
    };
    this.service.updateResource(item).subscribe(response => {
      this.global.iziToas.show({
        title: 'Pronto',
        message: 'Status atualizado com sucesso'
      });
    })
  }

  delete(id) {
    this.service.onDeleteConfirm(id).then(confirm => {
      if (confirm) {
        confirm.subscribe(response => {
          this.global.iziToas.success({ title: 'Registro removido com sucesso' });
          this.get();
        })
      }
    })
  }

}
