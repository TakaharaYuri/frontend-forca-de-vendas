import { Component, Input, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { EvoService } from 'src/app/@core/evo.service';
import { Global } from 'src/app/@core/global';

@Component({
  selector: 'app-modal-modelos',
  templateUrl: './modal-modelos.component.html',
  styleUrls: ['./modal-modelos.component.scss']
})
export class ModalModelosComponent implements OnInit {

  @Input() entity: any = {};
  public marcas;

  constructor(
    public global: Global,
    public service: EvoService,
    private activeModal: NgbActiveModal,
  ) {

  }

  ngOnInit(): void {
    this.getMarcas();
  }

  save() {
    this.service.entityName = 'gerencial/modelos';
    this.service.save(this.entity).subscribe(response => {
      this.global.iziToas.success({ title: 'Modelo cadastrado com Sucesso' });
      this.activeModal.close(true);
    },
      error => {
        this.global.iziToas.error({ title: 'Ocorreu um problema ao processar sua requisição' });
      })
  }

  getMarcas() {
    this.service.entityName = 'gerencial/marcas';
    this.service.getResources().subscribe(response => {
      this.marcas = response;
    })
  }

  close() {
    this.activeModal.close(false);
  }
}
