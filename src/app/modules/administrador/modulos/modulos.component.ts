import { Component, OnInit } from '@angular/core';
import { Global } from 'src/app/@core/global';
import { EvoService } from 'src/app/@core/evo.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalModulosComponent } from './modal-modulos/modal-modulos.component';

@Component({
  selector: 'app-modulos',
  templateUrl: './modulos.component.html',
  styleUrls: ['./modulos.component.css']
})
export class ModulosComponent implements OnInit {

  public data;

  constructor(
    public global: Global,
    public service: EvoService,
    private modalService: NgbModal
  ) {
    this.service.entityName = 'modulos';
  }

  ngOnInit(): void {
    this.get();
  }

  get() {
    this.service.getResources().subscribe(response => {
      this.data = response;
      console.log(response);
    })
  }

  open(data = {}) {
    const modal = this.modalService.open(ModalModulosComponent, {
      size: 'md',
      keyboard: false,
      backdrop: 'static'
    });

    modal.componentInstance.entity = data;

    modal.result.then((result) => {
      if (result) {
        this.get();
      }
    });
  }

}
