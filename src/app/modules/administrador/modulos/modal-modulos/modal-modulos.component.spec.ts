import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalModulosComponent } from './modal-modulos.component';

describe('ModalModulosComponent', () => {
  let component: ModalModulosComponent;
  let fixture: ComponentFixture<ModalModulosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalModulosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalModulosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
