import { Component, Input, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { EvoService } from 'src/app/@core/evo.service';
import { Global } from 'src/app/@core/global';

@Component({
  selector: 'app-modal-marcas',
  templateUrl: './modal-marcas.component.html',
  styleUrls: ['./modal-marcas.component.scss']
})
export class ModalMarcasComponent implements OnInit {

  @Input() entity: any = {};
  modulos;

  constructor(
    public global: Global,
    public service: EvoService,
    private activeModal: NgbActiveModal,
  ) { 
    
  }

  ngOnInit(): void {
    // this.getModules();
  }

  save() {
    this.service.entityName = 'gerencial/marcas';
    this.service.save(this.entity).subscribe(response => {
      this.global.iziToas.success({ title: 'Módulo cadastrado com Sucesso' });
      this.activeModal.close(true);
    },
      error => {
        this.global.iziToas.error({ title: 'Ocorreu um problema ao processar sua requisição' });
      })
  }

  close() {
    this.activeModal.close(false);
  }


}
