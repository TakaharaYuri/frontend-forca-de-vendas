import { Component, OnInit } from '@angular/core';
import { Global } from 'src/app/@core/global';

@Component({
  selector: 'app-administrador',
  templateUrl: './administrador.component.html',
  styleUrls: ['./administrador.component.css']
})
export class AdministradorComponent implements OnInit {

  public modulo;
  constructor(
    public global: Global
  ) { }

  ngOnInit(): void {
    this.modulo = this.global.getPermissions('administrador');
    console.log('Modulos ->', this.modulo);
  }
}
