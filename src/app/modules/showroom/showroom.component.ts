import { Component, OnInit } from '@angular/core';
import { Global } from 'src/app/@core/global';

@Component({
  selector: 'app-showroom',
  templateUrl: './showroom.component.html',
  styleUrls: ['./showroom.component.css']
})
export class ShowroomComponent implements OnInit {

  modulo;
  constructor(
    public global: Global
  ) { }

  ngOnInit(): void {
    this.modulo = this.global.getPermissions('showroom');
    console.log('Modulos ->', this.modulo);
  }

}
