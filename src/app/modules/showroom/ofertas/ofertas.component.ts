import { newArray } from '@angular/compiler/src/util';
import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { EvoService } from 'src/app/@core/evo.service';
import { Global } from 'src/app/@core/global';

@Component({
  selector: 'app-ofertas',
  templateUrl: './ofertas.component.html',
  styleUrls: ['./ofertas.component.css']
})
export class OfertasComponent implements OnInit {

  public data;
  public search;
  public filter: any = {};
  public page;
  public actualPage = 1;
  public lastPage;
  public totalPages;

  constructor(
    public global: Global,
    public service: EvoService,
    private modalService: NgbModal
  ) {

  }

  ngOnInit(): void {
    this.get();
  }

  get(page = 1) {
    this.actualPage = page;
    this.service.entityName = `showroom/ofertas?page=${this.actualPage}&${this.global.serialize(this.filter)}`;
    this.service.getResources().subscribe((response: any) => {
      this.data = response.data;
      this.lastPage = response.lastPage;
      this.totalPages = newArray(this.lastPage);
    })
  }

  delete(id) {
    this.service.entityName = 'showroom/ofertas';
    this.service.onDeleteConfirm(id).then(confirm => {
      if (confirm) {
        confirm.subscribe(response => {
          this.global.iziToas.success({ title: 'Registro removido com sucesso' });
          this.get();
        })
      }
    })
  }


  updateStatus(item) {
    this.service.entityName = 'showroom/ofertas';
    if (item.status == 1) {
      item.status = 0;
    }
    else {
      item.status = 1;
    };

    let data = {
      id: item.id,
      status: item.status
    }

    this.service.updateResource(data).subscribe(response => {
      this.global.iziToas.show({
        title: 'Pronto',
        message: 'Status atualizado com sucesso'
      });
    })
  }

  clear() {
    this.filter = {};
  }

}
