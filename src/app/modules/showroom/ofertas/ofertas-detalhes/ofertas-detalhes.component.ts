import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { EvoService } from 'src/app/@core/evo.service';
import { Global } from 'src/app/@core/global';

@Component({
  selector: 'app-ofertas-detalhes',
  templateUrl: './ofertas-detalhes.component.html',
  styleUrls: ['./ofertas-detalhes.component.css']
})
export class OfertasDetalhesComponent implements OnInit {

  public entity: any = {
    data_inicio: new Date().toISOString().substring(0, 10),
    status:1,
    meta_dados: {
      precos: {},
      configuracoes: {}
    }
  };
  public modeloSelecionado;
  public modelos;

  constructor(
    public global: Global,
    public service: EvoService,
    private modalService: NgbModal,
    public route: ActivatedRoute
  ) {
    this.service.entityName = 'showroom/ofertas';
    this.route.params.subscribe(params => {
      if (params.id) {
        this.get(params.id);
      }
    })
  }

  ngOnInit(): void {
    this.getModelos();
  }

  getModelos() {
    this.service.entityName = 'gerencial/modelos_empresa';
    this.service.getResources().subscribe(response => {
      this.modelos = response;
    })
  }

  save() {
    this.service.entityName = 'showroom/ofertas';
    if (this.entity.id) {
      this.service.updateResource(this.entity).subscribe(response => {
        if (response) {
          this.global.iziToas.success({
            title: 'Pronto',
            message: 'Oferta atualizada com sucesso'
          })
        }
      });
    }
    else {
      this.service.createResource(this.entity).subscribe(response => {
        if (response) {
          this.global.iziToas.success({
            title: 'Pronto',
            message: 'Oferta cadastrada com sucesso'
          })
        }
      });
    }
  }

  get(id) {
    this.service.entityName = 'showroom/ofertas';
    this.service.getResource(id).subscribe(response => {
      if (response) {
        this.entity = response;
        this.getModeloById(this.entity.modelo_id);
      }
    })
  }

  getModeloById(id) {
    this.service.entityName = 'gerencial/modelos';
    this.service.getResource(id).subscribe(response => {
      this.modeloSelecionado = response;
    });
  }

  resetTootleButtom(object) {
    if (this.entity.meta_dados.configuracoes.dePor) this.entity.meta_dados.configuracoes.dePor = false;
    if (this.entity.meta_dados.configuracoes.dePorEntrada) this.entity.meta_dados.configuracoes.dePorEntrada = false;
    if (this.entity.meta_dados.configuracoes.comEntrada) this.entity.meta_dados.configuracoes.comEntrada = false;
    if (this.entity.meta_dados.configuracoes.apenas) this.entity.meta_dados.configuracoes.apenas = false;
    if (this.entity.meta_dados.configuracoes.personalizado) this.entity.meta_dados.configuracoes.personalizado = false;
    object = true;
  }

}
