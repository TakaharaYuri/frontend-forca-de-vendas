import { Component, Input, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { EvoService } from 'src/app/@core/evo.service';
import { Global } from 'src/app/@core/global';

@Component({
  selector: 'app-modal-banners',
  templateUrl: './modal-banners.component.html',
  styleUrls: ['./modal-banners.component.css']
})
export class ModalBannersComponent implements OnInit {

  @Input() entity: any = {};

  constructor(
    public global: Global,
    public service: EvoService,
    private activeModal: NgbActiveModal,
  ) {
    
  }

  ngOnInit(): void {
    console.log(this.entity);
  }

  close() {
    this.activeModal.close(false);
  }

  save() {
    this.service.entityName = 'showroom/banners';
    if (this.entity.id) {
      this.service.updateResource(this.entity).subscribe(response => {
        this.close();
        this.global.iziToas.success({
          title: 'Pronto!',
          message: 'Banner atualizado com sucesso'
        });
      })
    }
    else {
      this.service.createResource(this.entity).subscribe(response => {
        this.close();
        this.global.iziToas.success({
          title: 'Pronto',
          message: 'Banner atualizado com sucesso'
        })
      })
    }
  }

}
