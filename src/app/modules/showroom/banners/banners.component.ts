import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { EvoService } from 'src/app/@core/evo.service';
import { Global } from 'src/app/@core/global';
import { ModalBannersComponent } from './modal-banners/modal-banners.component';

@Component({
  selector: 'app-banners',
  templateUrl: './banners.component.html',
  styleUrls: ['./banners.component.css']
})
export class BannersComponent implements OnInit {

  public banners;
  constructor(
    public service: EvoService,
    public global: Global,
    private modalService: NgbModal
  ) { }

  ngOnInit(): void {
    this.getBanners();
  }

  getBanners() {
    this.service.entityName = 'showroom/banners';
    this.service.getResources().subscribe(response => {
      this.banners = response;
    })
  }

  openBannerManager(data = {}) {
    const modal = this.modalService.open(ModalBannersComponent, {
      size: 'lg',
      keyboard: false,
      backdrop: 'static'
    });

    modal.componentInstance.entity = data;
    modal.result.then((result) => {
      this.getBanners();
    });
  }

  updateBannerStatus(item, $event) {
    this.service.entityName = 'showroom/banners';
    if (!item.vencido) {
      if (item.status == 1) {
        item.status = 0;
      }
      else {
        item.status = 1;
      };
      this.service.updateResource(item).subscribe(response => {
        this.global.iziToas.show({
          title: 'Pronto',
          message: 'Status atualizado com sucesso'
        });
      })
    }
    else {
      $event.value = false;
      this.global.iziToas.warning({
        title:'A data final do seu banner deve ser maior que a data de hoje',
        timeout:999999,
        maxWidth:'300px'
      })
    }
  }

  deleteBanner(id) {
    this.service.entityName = 'showroom/banners';
    this.service.onDeleteConfirm(id).then(confirm => {
      if (confirm) {
        confirm.subscribe(response => {
          this.global.iziToas.success({ title: 'Registro removido com sucesso' });
          this.getBanners();
        })
      }
    })
  }

}
