import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { EvoService } from 'src/app/@core/evo.service';
import { Global } from 'src/app/@core/global';
import { ModalBannersComponent } from '../banners/modal-banners/modal-banners.component';

@Component({
  selector: 'app-personalizacao',
  templateUrl: './personalizacao.component.html',
  styleUrls: ['./personalizacao.component.css']
})
export class PersonalizacaoComponent implements OnInit {

  public entity: any = {
    seo: {},
    configuracoes: {},
    layout: {
      inicio: {},
      oferta: {},
      footer: {},
      header: {},
      global: {}
    }
  }
  
  public activeTab = 1;
  public showroomUrl;


  constructor(
    public global: Global,
    public service: EvoService,
  ) {
    this.showroomUrl = `http://${this.global.empresa().host}`;
  }

  ngOnInit(): void {
    this.get();
  }

  get() {
    this.service.entityName = 'showroom/personalizar';
    this.service.getResources().subscribe(response => {
      if (response) {
        this.entity = response;
      }
    })
  }

 

  save() {
    this.service.entityName = `showroom/personalizar`;
    if (!this.entity.id) {
      this.service.createResource(this.entity).subscribe(response => {
        this.global.iziToas.success({
          title: 'Pronto',
          message: 'Informações atualizadas com sucesso'
        });
      })
    }
    else {
      this.service.updateResource(this.entity).subscribe(response => {
        this.global.iziToas.success({
          title: 'Pronto',
          message: 'Informações atualizadas com sucesso'
        });
      })
    }
  }


}
