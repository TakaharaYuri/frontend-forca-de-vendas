import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ShowroomRoutingModule } from './showroom-routing.module';
import { ShowroomComponent } from '../showroom/showroom.component';
import { FormsModule } from '@angular/forms';
import { MomentModule } from 'ngx-moment';
import { NgbDatepickerModule, NgbDropdownModule, NgbNavModule, NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';
import { YopsilonMaskModule } from 'yopsilon-mask';
import { PipesModule } from 'src/app/@core/pipes/pipes.module';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
import { CodeEditorModule } from '@ngstack/code-editor';
import { ColorPickerModule } from 'ngx-color-picker';
import { ComponentsModule } from 'src/app/components/components.module';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { NgxCurrencyModule } from 'ngx-currency';
import { OfertasComponent } from './ofertas/ofertas.component';
import { OfertasDetalhesComponent } from './ofertas/ofertas-detalhes/ofertas-detalhes.component';
import { TagInputModule } from 'ngx-chips';
import { PersonalizacaoComponent } from './personalizacao/personalizacao.component';
import { ModalBannersComponent } from './banners/modal-banners/modal-banners.component';
import { BannersComponent } from './banners/banners.component';
import { MatSlideToggleModule } from '@angular/material';




@NgModule({
  declarations: [
    ShowroomComponent, 
    OfertasComponent, 
    OfertasDetalhesComponent, 
    PersonalizacaoComponent, ModalBannersComponent, BannersComponent
  ],
  imports: [
    CommonModule,
    ShowroomRoutingModule,
    FormsModule,
    MomentModule,
    NgbTooltipModule,
    NgbDropdownModule,
    YopsilonMaskModule,
    PipesModule,
    NgxSkeletonLoaderModule,
    CodeEditorModule.forRoot(),
    ColorPickerModule,
    ComponentsModule,
    DragDropModule,
    NgxCurrencyModule,
    NgbNavModule,
    NgbDatepickerModule,
    NgbTooltipModule,
    TagInputModule,
    MatSlideToggleModule
  ]
})
export class ShowroomModule { }
