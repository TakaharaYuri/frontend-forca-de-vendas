import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OfertasDetalhesComponent } from './ofertas/ofertas-detalhes/ofertas-detalhes.component';
import { OfertasComponent } from './ofertas/ofertas.component';
import { ShowroomComponent } from './showroom.component';
import { PersonalizacaoComponent } from './personalizacao/personalizacao.component';
import { BannersComponent } from './banners/banners.component';


const routes: Routes = [
  {
    path: '',
    component: ShowroomComponent
  },
  {
    path: 'ofertas',
    component: OfertasComponent
  },
  {
    path: 'ofertas/detalhes',
    component: OfertasDetalhesComponent
  },
  {
    path: 'ofertas/detalhes/:id',
    component: OfertasDetalhesComponent
  },
  {
    path: "personalizar",
    component: PersonalizacaoComponent
  },
  {
    path: "banners",
    component: BannersComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ShowroomRoutingModule { }
