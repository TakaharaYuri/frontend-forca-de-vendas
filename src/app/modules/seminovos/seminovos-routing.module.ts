import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SeminovosAdicionarEstoqueModeloComponent } from './seminovos-estoque/seminovos-adicionar-estoque-modelo/seminovos-adicionar-estoque-modelo.component';
import { SeminovosAdicionarEstoqueComponent } from './seminovos-estoque/seminovos-adicionar-estoque/seminovos-adicionar-estoque.component';
import { SeminovosEstoqueComponent } from './seminovos-estoque/seminovos-estoque.component';
import { SeminovosComponent } from './seminovos.component';


const routes: Routes = [
  {
    path: '',
    component: SeminovosComponent
  },
  {
    path: 'estoque',
    component: SeminovosEstoqueComponent
  },
  {
    path: 'estoque/adicionar',
    component: SeminovosAdicionarEstoqueComponent
  },
  {
    path: 'estoque/adicionar/modelo',
    component: SeminovosAdicionarEstoqueModeloComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SeminovosRoutingModule { }
