import { Component, OnInit } from '@angular/core';
import { EvoService } from 'src/app/@core/evo.service';
import { Global } from 'src/app/@core/global';

@Component({
  selector: 'app-seminovos-adicionar-estoque-modelo',
  templateUrl: './seminovos-adicionar-estoque-modelo.component.html',
  styleUrls: ['./seminovos-adicionar-estoque-modelo.component.css']
})
export class SeminovosAdicionarEstoqueModeloComponent implements OnInit {

  public entity:any = {};
  public marcas;
  public type;

  constructor(
    public global: Global,
    public service: EvoService
  ) { }

  ngOnInit(): void {
    this.getMarcas();
  }

  getMarcas() {
    this.service.entityName = 'gerencial/marcas';
    this.service.getResources().subscribe(response => {
      this.marcas = response;
    })
  }

}
