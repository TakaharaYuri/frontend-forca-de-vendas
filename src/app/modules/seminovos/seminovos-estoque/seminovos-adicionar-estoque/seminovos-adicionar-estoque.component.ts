import { Component, OnInit } from '@angular/core';
import { EvoService } from 'src/app/@core/evo.service';
import { Global } from 'src/app/@core/global';

@Component({
  selector: 'app-seminovos-adicionar-estoque',
  templateUrl: './seminovos-adicionar-estoque.component.html',
  styleUrls: ['./seminovos-adicionar-estoque.component.css']
})
export class SeminovosAdicionarEstoqueComponent implements OnInit {

  public entity: any = {};
  constructor(
    public global: Global,
    public service: EvoService
  ) { }

  ngOnInit(): void {
  }

  getInformacaoPlaca(placa) {
    this.service.getPlaca(placa).subscribe((response: any) => {
      if (response) {
        this.entity = {
          placa: response.placa,
          cor: response.cor,
          municipio: response.municipio,
          situacao: response.situacao,
          codigo_situacao: response.codigoSituacao,
          uf: response.uf,
          chassi: response.chassi,
          ano_modelo: response.anoModelo,
          ano:response.ano,
          
        }
      }
    });
  }

}
