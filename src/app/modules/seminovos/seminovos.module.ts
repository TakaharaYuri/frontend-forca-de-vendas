import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SeminovosRoutingModule } from './seminovos-routing.module';
import { FormsModule } from '@angular/forms';
import { MomentModule } from 'ngx-moment';
import { NgbDatepickerModule, NgbDropdownMenu, NgbDropdownModule, NgbNavModule, NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';
import { YopsilonMaskModule } from 'yopsilon-mask';
import { PipesModule } from 'src/app/@core/pipes/pipes.module';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
import { CodeEditorModule } from '@ngstack/code-editor';
import { ColorPickerModule } from 'ngx-color-picker';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { MatSlideToggleModule } from '@angular/material';
import { TagInputModule } from 'ngx-chips';
import { NgxCurrencyModule } from 'ngx-currency';
import { ComponentsModule } from 'src/app/components/components.module';
import { SeminovosComponent } from './seminovos.component';
import { SeminovosEstoqueComponent } from './seminovos-estoque/seminovos-estoque.component';
import { CardSeminovoModelComponent } from './seminovos-estoque/card-seminovo-model/card-seminovo-model.component';
import { SeminovosAdicionarEstoqueComponent } from './seminovos-estoque/seminovos-adicionar-estoque/seminovos-adicionar-estoque.component';
import { SeminovosAdicionarEstoqueModeloComponent } from './seminovos-estoque/seminovos-adicionar-estoque-modelo/seminovos-adicionar-estoque-modelo.component';
import { NgSelectModule } from '@ng-select/ng-select';


@NgModule({
  declarations: [SeminovosComponent, SeminovosEstoqueComponent, CardSeminovoModelComponent, SeminovosAdicionarEstoqueComponent, SeminovosAdicionarEstoqueModeloComponent],
  imports: [
    CommonModule,
    SeminovosRoutingModule,
    FormsModule,
    MomentModule,
    NgbTooltipModule,
    NgbDropdownModule,
    YopsilonMaskModule,
    PipesModule,
    NgxSkeletonLoaderModule,
    CodeEditorModule.forRoot(),
    ColorPickerModule,
    ComponentsModule,
    DragDropModule,
    NgxCurrencyModule,
    NgbNavModule,
    NgbDatepickerModule,
    NgbTooltipModule,
    NgSelectModule,
    TagInputModule,
    MatSlideToggleModule,
    NgbDropdownModule,
  ]
})
export class SeminovosModule { }
