import { Component, OnInit, Input } from '@angular/core';
import { Global } from 'src/app/@core/global';
import { EvoService } from 'src/app/@core/evo.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-modal-condicionar-etapa',
  templateUrl: './modal-condicionar-etapa.component.html',
  styleUrls: ['./modal-condicionar-etapa.component.css']
})
export class ModalCondicionarEtapaComponent implements OnInit {

  @Input() entity: any = {};
  @Input() etapas: any;
  @Input() item: any = {};
  mainEtapaIndex;
  conditions = [{}];

  constructor(
    public global: Global,
    public service: EvoService,
    private activeModal: NgbActiveModal,
  ) { }
  ngOnInit(): void {
    console.log('Entity', this.entity);
    console.log('Item', this.item);

    this.mainEtapaIndex = this.etapas.indexOf(this.item);
    if (!this.item.conditions) {
      this.entity.conditions = [];
      this.item.conditions = [];
      this.item.selected_options = [];
    }
    else {
      // this.item = JSON.parse(JSON.stringify(this.item));
      // this.item.selected_options = JSON.parse(JSON.stringify(this.item.selected_options));
      // this.item.selected_options = JSON.parse(JSON.stringify(this.entity.selected_options)); 
      // this.entity.conditions.map( (item, index) => {
      //   this.setCondition(item, index, this.item.selected_options[index]);
      // })
    }
  }

  setCondition(option, option_idx, etapa_idx) {
    console.log('->', etapa_idx);
    if (etapa_idx != false) {
      if (!this.item.conditions) this.item.conditions = [];
      this.item.selected_options[option_idx] = etapa_idx;

      let i = this.item.conditions.find(item => item.option == option);

      if (i) {
        i.etapa = this.etapas[etapa_idx];
      }
      else {
        this.item.conditions.push({
          id: option_idx,
          option: option,
          etapa: this.etapas[etapa_idx]
        });
      }
    }
    else {
      this.item.conditions = null;
    }
  }

  close() {
    this.activeModal.close(false);
  }

  save() {
    this.activeModal.close(this.item);
  }

}
