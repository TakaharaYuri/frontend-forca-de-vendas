import { Component, OnInit } from '@angular/core';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { Global } from 'src/app/@core/global';
import { EvoService } from 'src/app/@core/evo.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalItemFormularioComponent } from './modal-item-formulario/modal-item-formulario.component';
import { ModalCondicionarEtapaComponent } from './modal-condicionar-etapa/modal-condicionar-etapa.component';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-items-formulario',
  templateUrl: './items-formulario.component.html',
  styleUrls: ['./items-formulario.component.css']
})
export class ItemsFormularioComponent implements OnInit {
  etapas = [];
  formularioId;
  isNewFormItems = true;


  constructor(
    public global: Global,
    public service: EvoService,
    private modalService: NgbModal,
    public route: ActivatedRoute

  ) {
    this.route.params.subscribe(param => {
      if (param.formulario_id) {
        this.formularioId = param.formulario_id;
        this.get();
      }
    })
  }

  ngOnInit(): void {

  }

  get() {
    this.service.entityName = `marketing/formulario-items?formulario_id=${this.formularioId}`;
    this.service.getResources().subscribe((response: any) => {
      if (response.length > 0) this.isNewFormItems = false;
      this.etapas = response;
    });
  }

  save() {
    console.log(this.isNewFormItems);
    if (this.isNewFormItems) {
      this.service.createResource({ etapas: this.etapas }).subscribe(response => {
        this.global.iziToas.success({ title: 'Formulário criado com sucesso' });
        this.get();
      })
    }
    else {
      this.service.entityName = `marketing/formulario-items/${this.formularioId}?formulario_id=${this.formularioId}`;
      this.service.updateResource({ etapas: this.etapas }).subscribe(response => {
        this.global.iziToas.success({ title: 'Formulário atualizado com sucesso' });
        this.get();
      })
    }

    localStorage.setItem('formulario', JSON.stringify(this.etapas));
  }

  drop(event: CdkDragDrop<string[]>) {
    console.log('EVENT ->', event);
    moveItemInArray(this.etapas, event.previousIndex, event.currentIndex);
    if (this.etapas[event.currentIndex].id) {
      this.save();
    }

  }

  open(entity = {}) {
    const modal = this.modalService.open(ModalItemFormularioComponent, {
      size: 'md',
      keyboard: false,
      backdrop: 'static'
    });

    modal.componentInstance.entity = entity;

    modal.result.then((result) => {
      console.log(result);
      if (result) {
        const checkFormFieldExists = this.etapas.indexOf(result);
        console.log(checkFormFieldExists);
        if (checkFormFieldExists == -1) {
          this.etapas.push(result);
        }
        this.save();
      }
    });
  }

  conditions(item) {
    const modal = this.modalService.open(ModalCondicionarEtapaComponent, {
      size: 'md',
      keyboard: false,
      backdrop: 'static'
    });

    modal.componentInstance.item = item;
    modal.componentInstance.etapas = this.etapas;

    modal.result.then((result) => {
      console.log(result);
      if (result) {
        const indexField = this.etapas.indexOf(result);
        console.log(indexField);
        this.etapas[indexField] = result;
        this.save();
      }
      // if (result) {
      //   const checkFormFieldExists = this.etapas.findIndex(item => item.id = result.id);
      //   console.log(checkFormFieldExists);

      //   // if (checkFormFieldExists == -1) {
      //   //   console.log(result);
      //   //   this.etapas.push(result);
      //   // }
      //   // else {
      //   //   this.etapas[checkFormFieldExists] = result;
      //   // }

      //   this.save();
      // }
    });
  }

  delete(item) {
    if (item.id) {
      this.service.entityName = `marketing/formulario-items/${item.id}?formulario_id=${this.formularioId}`;
      this.service.onDeleteConfirm().then(confirm => {
        if (confirm) {
          confirm.subscribe(response => {
            this.global.iziToas.success({ title: 'Registro removido com sucesso' });
            this.get();
          })
        }
      });
    }
    else {
      let index = this.etapas.indexOf(item);
      this.etapas.splice(index, 1);
    }
  }
}
