import { Component, OnInit, Input } from '@angular/core';
import { Global } from 'src/app/@core/global';
import { EvoService } from 'src/app/@core/evo.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-modal-item-formulario',
  templateUrl: './modal-item-formulario.component.html',
  styleUrls: ['./modal-item-formulario.component.css']
})
export class ModalItemFormularioComponent implements OnInit {

  @Input() entity: any = {};
  options = ['Opção 1'];

  constructor(
    public global: Global,
    public service: EvoService,
    private activeModal: NgbActiveModal,
  ) { }

  ngOnInit(): void {
    if (this.entity.options) {
      //Estamos fazendo isso por que o Options está assumindo o mesmo endereço de memoria do entity
      this.options = JSON.parse(JSON.stringify(this.entity.options));
    }
    else {
      this.options = ['Opção 1'];
      this.entity.options = [];
    }

    if (!this.entity.enabled) {this.entity.enabled = true;}
    // console.log()

  }

  close() {
    this.activeModal.close(false);
  }

  save() {
    this.activeModal.close(this.entity);
  }

  addOption() {
    let option = '';
    this.options.push(option);
  }

  removeOption(index) {
    console.log(this.options, index);
    this.options.splice(index, 1);
  }

  generateIdentifier(text) {
    text = text.replace(/[^\w\s]/gi, '');
    text = text.split('  ').join('');
    text = text.split('-').join('');
    text = text.split(' ').join('');
    text = text.split('--').join('');
    text = text.toLowerCase();
    console.log(text);
    this.entity.identifier = text.split(' ').join('');
  }
}
