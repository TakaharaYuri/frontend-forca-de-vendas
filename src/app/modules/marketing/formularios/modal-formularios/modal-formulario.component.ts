import { Component, OnInit, Input } from '@angular/core';
import { Global } from 'src/app/@core/global';
import { EvoService } from 'src/app/@core/evo.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-modal-formulario',
  templateUrl: './modal-formulario.component.html',
  styleUrls: ['./modal-formulario.component.css']
})
export class ModalFormularioComponent implements OnInit {
  @Input() entity:any = {};

  constructor(
    public global: Global,
    public service: EvoService,
    private activeModal: NgbActiveModal,
  ) { }

  ngOnInit(): void {
  }

  close() {
    this.activeModal.close(false);
  } 

  save() {
    this.service.entityName = 'marketing/formulario';
    this.service.save(this.entity).subscribe( response => {
      this.global.iziToas.success({title:'Formulario cadastrado com Sucesso'});
      this.activeModal.close(true);
    },
    error => {
      this.global.iziToas.error({title:'Ocorreu um problema ao processar sua requisição'});
    })
  }

}
