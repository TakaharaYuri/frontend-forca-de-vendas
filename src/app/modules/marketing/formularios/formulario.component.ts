import { Component, OnInit } from '@angular/core';
import { Global } from 'src/app/@core/global';
import { EvoService } from 'src/app/@core/evo.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalFormularioComponent } from './modal-formularios/modal-formulario.component';

@Component({
  selector: 'app-formulario',
  templateUrl: './formulario.component.html',
  styleUrls: ['./formulario.component.scss']
})
export class FormularioComponent implements OnInit {

  public data;
  constructor(
    public global: Global,
    public service: EvoService,
    private modalService: NgbModal
  ) {

  }

  ngOnInit(): void {
    this.get();
  }

  get() {
    this.service.entityName = 'marketing/formulario';
    this.service.getResources().subscribe( response => {
      this.data = response;
    })
  }

  delete(id) {
    this.service.onDeleteConfirm(id).then(confirm => {
      if (confirm) {
        confirm.subscribe( response => {
          this.global.iziToas.success({title:'Registro removido com sucesso'});
          this.get();
        })
      }
    })
  }

  open(entity = {}) {
    const modal = this.modalService.open(ModalFormularioComponent, {
      size: 'sm',
      keyboard: false,
      backdrop: 'static'
    });

    modal.componentInstance.entity = entity;

    modal.result.then((result) => {
      if (result) {
        this.get();
      }
    });
  }


}
