import { Component, OnInit } from '@angular/core';
import { Global } from 'src/app/@core/global';

@Component({
  selector: 'app-marketing',
  templateUrl: './marketing.component.html',
  styleUrls: ['./marketing.component.css']
})
export class MarketingComponent implements OnInit {
  modulo;
  constructor(
    public global: Global
  ) { }

  ngOnInit(): void {
    this.modulo = this.global.getPermissions('marketing');
    console.log('Modulos ->', this.modulo);
  }


}
