import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MarketingComponent } from './marketing.component';
import { PaginaContatoComponent } from './pagina-contato/pagina-contato.component';
import { FormularioComponent } from './formularios/formulario.component';
import { ItemsFormularioComponent } from './formularios/items-formularios/items-formulario.component';
import { LeadsComponent } from './leads/leads.component';
import { ListarLeadsComponent } from './leads/listar-leads/listar-leads.component';
import { PastasLeadsComponent } from './leads/pastas-leads/pastas-leads.component';


const routes: Routes = [
  { path: '', component: MarketingComponent },
  { path: 'pagina-contato', component: PaginaContatoComponent },
  { path: 'formularios', component: FormularioComponent },
  { path: 'formularios/:formulario_id/items', component: ItemsFormularioComponent },
  { path: 'leads', component: LeadsComponent },
  { path: 'leads/listar-leads', component: ListarLeadsComponent },
  { path: 'leads/listar-leads/:folderId', component: ListarLeadsComponent },
  { path: 'leads/pastas', component: PastasLeadsComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MarketingRoutingModule { }

