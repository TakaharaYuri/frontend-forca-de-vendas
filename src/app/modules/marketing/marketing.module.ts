import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MarketingRoutingModule } from './marketing-routing.module';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
import { PipesModule } from 'src/app/@core/pipes/pipes.module';
import { YopsilonMaskModule } from 'yopsilon-mask';
import { NgbTooltipModule, NgbDropdownModule, NgbTabsetModule, NgbNavModule } from '@ng-bootstrap/ng-bootstrap';
import { MomentModule } from 'ngx-moment';
import { FormsModule } from '@angular/forms';
import { MarketingComponent } from './marketing.component';
import { PaginaContatoComponent } from './pagina-contato/pagina-contato.component';
import { CodeEditorModule } from '@ngstack/code-editor';
import { ColorPickerModule } from 'ngx-color-picker';
import { FormularioComponent } from './formularios/formulario.component';
import { ComponentsModule } from 'src/app/components/components.module';
import { ModalFormularioComponent } from './formularios/modal-formularios/modal-formulario.component';
import { ItemsFormularioComponent } from './formularios/items-formularios/items-formulario.component';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { ModalItemFormularioComponent } from './formularios/items-formularios/modal-item-formulario/modal-item-formulario.component';
import { ModalCondicionarEtapaComponent } from './formularios/items-formularios/modal-condicionar-etapa/modal-condicionar-etapa.component';
import { LeadsComponent } from './leads/leads.component';
import { ListarLeadsComponent } from './leads/listar-leads/listar-leads.component';
import { PastasLeadsComponent } from './leads/pastas-leads/pastas-leads.component';
import { ModalPastasLeadsComponent } from './leads/pastas-leads/modal-pastas-leads/modal-pastas-leads.component';
import { ModalEtiquetasComponent } from './leads/listar-leads/modal-etiquetas/modal-etiquetas.component';
import { NgxCurrencyModule } from 'ngx-currency';






@NgModule({
  imports: [
    CommonModule,
    MarketingRoutingModule,
    FormsModule,
    MomentModule,
    NgbTooltipModule,
    NgbDropdownModule,
    YopsilonMaskModule,
    PipesModule,
    NgxSkeletonLoaderModule,
    CodeEditorModule.forRoot(),
    ColorPickerModule,
    ComponentsModule,
    DragDropModule,
    NgxCurrencyModule,
    NgbNavModule
  ],
  declarations: [
    MarketingComponent,
    PaginaContatoComponent,
    FormularioComponent,
    ModalFormularioComponent,
    ItemsFormularioComponent,
    ModalItemFormularioComponent,
    ModalCondicionarEtapaComponent, 
    LeadsComponent, 
    ListarLeadsComponent, 
    PastasLeadsComponent, 
    ModalPastasLeadsComponent, 
    ModalEtiquetasComponent
  ],
})
export class MarketingModule { }
