import { Component, OnInit } from '@angular/core';
import { EvoService } from 'src/app/@core/evo.service';
import { Global } from 'src/app/@core/global';
import { ActivatedRoute } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalEtiquetasComponent } from './modal-etiquetas/modal-etiquetas.component';
import { KeyCode } from '@ng-select/ng-select/lib/ng-select.types';

@Component({
  selector: 'app-listar-leads',
  templateUrl: './listar-leads.component.html',
  styleUrls: ['./listar-leads.component.css']
})
export class ListarLeadsComponent implements OnInit {

  public data;
  public folderId;
  public etiquetas;
  constructor(
    public service: EvoService,
    public global: Global,
    public router: ActivatedRoute,
    private modalService: NgbModal
  ) {
    console.log('[ListarLeadsComponent] init');
    this.router.params.subscribe(param => {
      if (param.folderId) {
        this.folderId = param.folderId;
      }
    })
  }

  ngOnInit(): void {
    this.get();
    this.getEtiquetas();
  }

  get() {
    this.service.entityName = 'marketing/leads';
    if (this.folderId) {
      this.service.entityName = `marketing/leads?folder=${this.folderId}`;
    }
    this.service.getResources().subscribe((response: any) => {
      this.data = response.data;
    });
  }

  openEtiquetasLeads(lead = {}) {
    const modal = this.modalService.open(ModalEtiquetasComponent, {
      size: 'sm',
      keyboard: false,
      backdrop: 'static'
    });
    modal.componentInstance.lead = lead;

    modal.result.then((result) => {
      if (result) {
        this.getEtiquetas();
      }
    });
  }

  getEtiquetas() {
    this.service.entityName = 'marketing/leads-etiquetas';
    this.service.getResources().subscribe(response => {
      this.etiquetas = response;
    });
  }

  editEtiqueta(item) {
    item.onEdit = true;
  }

  onUpdateEtiqueta(event: any = {}, item) {
    if (event) {
      if (event.code == 'Escape') {
        item.onEdit = false;
      }
      else if (event.code == '') {

      }
    }
    else {
      item.onLoading = true;
      console.log('ITEM ->', item);
      this.service.entityName = 'marketing/leads-etiquetas';
      this.service.updateResource(item).subscribe(response => {
        item.onLoading = false;
        item.onEdit = false;
        this.global.iziToas.show({
          title: 'Sucesso',
          message: 'Etiqueta atualizada com sucesso'
        })
      })
    }
  }

  addEtiquetaInLead(lead, etiqueta) {
    const etiquetaExists = lead.etiquetas.find(item => item.id == etiqueta.id);
    if (!etiquetaExists) {
      if (lead.etiquetas.length <= 1) {
        lead.etiquetas.push(etiqueta);
        this.service.entityName = `marketing/add-etiqueta-lead/${lead.id}/${etiqueta.id}`;
        this.service.getResources().subscribe(response => {
          console.log('Response ->', response);
        });
      }
      else {
        this.global.iziToas.warning({
          title: 'Ops',
          message: 'Você só pode adicionar no máximo 2 etiquetas por Lead'
        });
      }
    }
    else {
      const etiquetaIndex = lead.etiquetas.indexOf(etiqueta);
      console.log("INDEX", etiquetaIndex);
      lead.etiquetas.splice(etiquetaIndex, 1);
      this.service.entityName = `marketing/add-etiqueta-lead/${lead.id}/${etiqueta.id}`;
      this.service.getResources().subscribe(response => {
        console.log('Response ->', response);
      });
    }
  }

  onRemoveEtiqueta(id) {
    this.service.entityName = 'marketing/leads-etiquetas';
    this.service.onDeleteConfirm(id, 'Você realmente deseja apagar esta etiqueta? Ela será removida de todos os Leads').then(confirm => {
      if (confirm) {
        confirm.subscribe(response => {
          this.global.iziToas.success({ title: 'Registro removido com sucesso' });
          this.getEtiquetas();
          this.get();
        })
      }
    })
  }

}
