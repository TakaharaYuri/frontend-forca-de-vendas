import { Component, OnInit, Input } from '@angular/core';
import { Global } from 'src/app/@core/global';
import { EvoService } from 'src/app/@core/evo.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-modal-etiquetas',
  templateUrl: './modal-etiquetas.component.html',
  styleUrls: ['./modal-etiquetas.component.css']
})
export class ModalEtiquetasComponent implements OnInit {

  @Input() entity: any = {};
  public loading = false;

  constructor(
    public global: Global,
    public service: EvoService,
    private activeModal: NgbActiveModal,
  ) { }

  ngOnInit(): void {
  }

  close() {
    this.activeModal.close(false);
  }

  save() {
    this.loading = true;
    this.service.entityName = 'marketing/leads-etiquetas';
    this.service.createResource(this.entity).subscribe(response => {
      if (response) {
        this.activeModal.close(true);
        this.loading = false;
        this.global.iziToas.show({
          title:'Pronto',
          message:'Etiqueta criada com Sucesso'
        })
      }
    });
  }
  // getEtiquetas() {
  //   this.service.entityName = 'marketing/leads-etiquetas';
  //   this.service.getResources().subscribe(response => {
  //     this.etiquetas = response;
  //   });
  // }
  // getEtiquetasLeads() {
  //   this.service.entityName = 'marketing/leads-etiquetas-listar';
  //   this.service.getResource(this.lead.id).subscribe(response => {
  //     this.etiquetasLeads = response;
  //   });
  // }

}
