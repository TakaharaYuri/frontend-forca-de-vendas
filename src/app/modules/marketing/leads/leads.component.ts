import { Component, OnInit } from '@angular/core';
import { Global } from 'src/app/@core/global';
import { EvoService } from 'src/app/@core/evo.service';

@Component({
  selector: 'app-leads',
  templateUrl: './leads.component.html',
  styleUrls: ['./leads.component.css']
})
export class LeadsComponent implements OnInit {
  public pastas;
  constructor(
    public global: Global,
    public service: EvoService,
  ) { }

  ngOnInit(): void {
    this.getPastas();
  }

  getPastas() {
    this.service.entityName = 'marketing/leads-pastas';
    this.service.getResources().subscribe( response => {
      this.pastas = response;
    })
  }

}
