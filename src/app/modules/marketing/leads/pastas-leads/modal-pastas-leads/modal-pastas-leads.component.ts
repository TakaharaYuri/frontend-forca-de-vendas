import { Component, OnInit, Input } from '@angular/core';
import { Global } from 'src/app/@core/global';
import { EvoService } from 'src/app/@core/evo.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-modal-pastas-leads',
  templateUrl: './modal-pastas-leads.component.html',
  styleUrls: ['./modal-pastas-leads.component.css']
})
export class ModalPastasLeadsComponent implements OnInit {

  @Input() entity: any = {};
  constructor(
    public global: Global,
    public service: EvoService,
    public activeModal: NgbActiveModal
  ) { }

  ngOnInit(): void {
  }

  save() {
    this.service.entityName = 'marketing/leads-pastas';
    this.service.save(this.entity).subscribe(response => {
      this.global.iziToas.success({ title: 'Pasta cadastrado com Sucesso' });
      this.activeModal.close(true);
    },
      error => {
        this.global.iziToas.error({ title: 'Ocorreu um problema ao processar sua requisição' });
      })
  }

  close() {
    this.activeModal.close(false);
  }

}
