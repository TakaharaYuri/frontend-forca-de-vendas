import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { EvoService } from 'src/app/@core/evo.service';
import { Global } from 'src/app/@core/global';
import { CodeModel } from '@ngstack/code-editor';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-pagina-contato',
  templateUrl: './pagina-contato.component.html',
  styleUrls: ['./pagina-contato.component.css']
})
export class PaginaContatoComponent implements OnInit {

  public apiUrl;
  public active;
  public formularios;
  @ViewChild('form') form: ElementRef;
  public entity: any = {
    seo: {},
    configuracoes: {},
    layout: {
      custom_css: {},
      show_legal_text: true,
      show_address: true,
      show_cnpj: true,
      show_image_products: true

    }
  };


  constructor(
    public global: Global,
    public service: EvoService
  ) {
    this.apiUrl = environment.host;
    console.log(`Empresa ->`, this.global.empresa().id);
  }

  ngOnInit(): void {
    this.getData();
    this.getFormularios();
  }

  getData() {
    this.service.entityName = 'marketing/pagina-contato';
    this.service.getResources().subscribe((response: any) => {
      if (response) {
        this.entity = response;
        if (!this.entity.seo) this.entity.seo = {};
        if (!this.entity.configuracoes) this.entity.configuracoes = {};
        // if (!this.entity.layout) this.entity.layout = {};
      }
    })
  }

  save() {
    this.service.entityName = 'marketing/pagina-contato';
    if (this.entity.id) {
      this.service.updateResource(this.entity).subscribe(response => {
        this.global.iziToas.success({ title: 'Dados atualizados com sucesso' })
      });
    }
    else {
      this.service.createResource(this.entity).subscribe(response => {
        this.global.iziToas.success({ title: 'Dados salvos com sucesso' })
      })
    }
  }

  getFormularios() {
    this.service.entityName = 'marketing/formulario';
    this.service.getResources().subscribe(response => {
      this.formularios = response;
    });
  }

  onCodeChanged(value) {
    this.entity.layout.custom_css = value;
  }

  setImage(entity, image) {
    entity = image;
  }

}
