import { Component, OnInit } from '@angular/core';
import { Global } from 'src/app/@core/global';
import { EvoService } from 'src/app/@core/evo.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalSetorComponent } from './modal-setor/modal-setor.component';



@Component({
  selector: 'app-setor',
  templateUrl: './setor.component.html',
  styleUrls: ['./setor.component.css']
})
export class SetorComponent implements OnInit {

  public data;

  constructor(
    public global: Global,
    public service: EvoService,
    private modalService: NgbModal
  ) {

  }

  ngOnInit(): void {
    this.get();
  }

  get() {
    this.service.entityName = 'setor';
    this.service.getResources().subscribe(response => {
      this.data = response;
      console.log('Response ->', response);
    })
  }

  delete(id) {
    this.service.onDeleteConfirm(id).then(confirm => {
      if (confirm) {
        confirm.subscribe(response => {
          console.log('DELETE', response);
          this.global.iziToas.success({ title: 'Registro removido com sucesso' });
          this.get();
        })
      }
    })
  }

  open(data = {}) {
    const modal = this.modalService.open(ModalSetorComponent, {
      size: 'md',
      keyboard: false,
      backdrop: 'static'
    });

    modal.componentInstance.entity = data;

    modal.result.then((result) => {
      if (result) {
        this.get();
      }
    });
  }

}
