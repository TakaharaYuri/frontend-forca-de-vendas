import { Component, OnInit, Input } from '@angular/core';
import { Global } from 'src/app/@core/global';
import { EvoService } from 'src/app/@core/evo.service';
import { HttpEventType, HttpErrorResponse } from '@angular/common/http';
import { catchError, map } from 'rxjs/operators';
import { of } from 'rxjs';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-usuario-detalhes',
  templateUrl: './usuario-detalhes.component.html',
  styleUrls: ['./usuario-detalhes.component.css']
})
export class UsuarioDetalhesComponent implements OnInit {

  public entity: any = {};
  public setores;
  public img;
  public id;
  public passwordConfirmation;

  constructor(
    public service: EvoService,
    public global: Global,
    public route: ActivatedRoute
  ) {
    this.getSetores();
    this.route.params.subscribe(param => {
      if (param.id) {
        this.id = param.id;
      }
    });
   }

  ngOnInit(): void {
    if (this.id) this.getUserData();
  }

  getSetores() {
    this.service.entityName = 'setor';
    this.service.getResources().subscribe(response => {
      this.setores = response;
    })
  }

  getUserData() {
    this.service.entityName = 'usuario';
    this.service.getResource(this.id).subscribe(response => {
      this.entity = response;
    })
  }

  save() {
    this.service.entityName = 'usuario';
    this.service.save(this.entity).subscribe(response => {
      this.global.iziToas.success({
        title: 'Dados salvos com sucesso'
      });
    })
  }

  loadImage(image) {
    let tmpFile = image.target.files.item(0)
    this.uploadImage(tmpFile);

    if (FileReader) {
      var fr = new FileReader();
      fr.onload = () => {
        this.img = fr.result;
      }

      fr.readAsDataURL(tmpFile);
    }

    else {
      this.global.iziToas.warning({ title: 'Metodo não compátivel com sua versão' });
    }
  }

  uploadImage(file) {
    const formData = new FormData();
    formData.append('image', file);
    file.inProgress = true;
    this.service.upload(formData).pipe(
      map(event => {
        switch (event.type) {
          case HttpEventType.UploadProgress:
            file.progress = Math.round(event.loaded * 100 / event.total);
            console.log('file', file);
            break;
          case HttpEventType.Response:
            console.log('event', event);
            return event;
        }
      }),
      catchError((error: HttpErrorResponse) => {
        file.inProgress = false;
        return of(`${file.name} upload failed.`);
      })).subscribe((event: any) => {
        if (typeof (event) === 'object') {
          this.entity.imagem = event.body.fileName;
        }
      });
  }

}
