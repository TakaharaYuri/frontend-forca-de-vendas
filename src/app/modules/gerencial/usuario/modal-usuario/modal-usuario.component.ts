import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Global } from 'src/app/@core/global';
import { EvoService } from 'src/app/@core/evo.service';
import { HttpEventType, HttpErrorResponse } from '@angular/common/http';
import { catchError, map } from 'rxjs/operators';
import { of } from 'rxjs';

@Component({
  selector: 'app-modal-usuario',
  templateUrl: './modal-usuario.component.html',
  styleUrls: ['./modal-usuario.component.css']
})
export class ModalUsuarioComponent implements OnInit {

  @Input() entity: any = {};
  public setores;
  public img;

  constructor(
    public modal: NgbActiveModal,
    public global: Global,
    public service: EvoService
  ) { }

  ngOnInit(): void {
    this.getSetores();
  }

  public getSetores() {
    this.service.entityName = 'setor';
    this.service.getResources().subscribe(response => {
      this.setores = response;
    })
  }

  public save() {
    this.service.entityName = 'usuario';
    this.service.save(this.entity).subscribe(response => {
      this.global.iziToas.success({
        title: 'Usuário cadastrado com Sucesso'
      });
      this.modal.close();
    })
  }

  loadImage(image) {
    let tmpFile = image.target.files.item(0)
    this.uploadImage(tmpFile);

    if (FileReader) {
      var fr = new FileReader();
      fr.onload = () => {
        this.img = fr.result;
      }

      fr.readAsDataURL(tmpFile);
    }

    else {
      this.global.iziToas.warning({ title: 'Metodo não compátivel com sua versão' });
    }
  }

  uploadImage(file) {
    const formData = new FormData();
    formData.append('image', file);
    file.inProgress = true;
    this.service.upload(formData).pipe(
      map(event => {
        switch (event.type) {
          case HttpEventType.UploadProgress:
            file.progress = Math.round(event.loaded * 100 / event.total);
            console.log('file', file);
            break;
          case HttpEventType.Response:
            console.log('event', event);
            return event;
        }
      }),
      catchError((error: HttpErrorResponse) => {
        file.inProgress = false;
        return of(`${file.name} upload failed.`);
      })).subscribe((event: any) => {
        if (typeof (event) === 'object') {
          this.entity.imagem = event.body.fileName;
        }
      });
  }

}
