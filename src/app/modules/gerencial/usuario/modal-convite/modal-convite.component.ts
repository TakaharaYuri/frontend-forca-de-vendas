import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Global } from 'src/app/@core/global';
import { EvoService } from 'src/app/@core/evo.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-modal-convite',
  templateUrl: './modal-convite.component.html',
  styleUrls: ['./modal-convite.component.css']
})
export class ModalConviteComponent implements OnInit {
  setores;
  @Input() entity: any = {};
  error = false;

  constructor(
    public modal: NgbActiveModal,
    public global: Global,
    public service: EvoService
  ) { }

  ngOnInit(): void {
    this.getSetores();
    this.entity.status = 3;
  }

  public getSetores() {
    this.service.entityName = 'setor';
    this.service.getResources().subscribe(response => {
      this.setores = response;
    })
  }

  public close() {
    this.modal.close();
  }

  public checkEmailExists(email) {
    const regexValidate = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (regexValidate.test(String(email).toLowerCase())) {
      this.service.entityName = `verificar/email-usuario?email=${email}`;
      this.service.getResources().subscribe((response: any) => {
        if (response.result) {
          this.error = true;
          Swal.fire({
            icon: 'warning',
            title: 'Anteção',
            text: response.msg
          }).then((result) => {
            if (result.value) {
              if (response.reason == 'user_same_exists' || response.reason == 'blocked') {
                this.modal.close();
              }
              else if (response.reason == 'user_exists') {

              }
              else if (response.reason = 'not_activate') {

              }
            }
          })
        }
      })
    }
  }

  public sendInvitation() {
    this.service.entityName = 'acoes/enviar-convite';
    this.service.createResource(this.entity).subscribe((response: any) => {
      if (response.result) {
        this.global.iziToas.success({ title: 'Convite enviado com sucesso' });
        this.modal.close();
      }
      else {
        this.global.iziToas.success({ title: response.msg });
      }
    })
  }

}
