import { Component, OnInit } from '@angular/core';
import { EvoService } from 'src/app/@core/evo.service';
import { Global } from 'src/app/@core/global';
import { HttpEventType, HttpErrorResponse } from '@angular/common/http';
import { catchError, map } from 'rxjs/operators';
import { of } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { BlockUI, NgBlockUI } from 'ng-block-ui';

@Component({
  selector: 'app-empresa-detalhes',
  templateUrl: './empresa-detalhes.component.html',
  styleUrls: ['./empresa-detalhes.component.css']
})
export class EmpresaDetalhesComponent implements OnInit {

  public id;
  @BlockUI() blockUI: NgBlockUI;
  public entity: any = {};
  public img;
  public marcas;

  constructor(
    private global: Global,
    private service: EvoService,
    public route: ActivatedRoute,
    public router: Router
  ) {
    this.route.params.subscribe(param => {
      if (param.id) {
        this.id = param.id;
      }
    });
  }

  ngOnInit(): void {
    this.getMarcas();
    if (this.id) this.get();
    
  }

  public get() {
    this.service.entityName = 'empresa';
    this.service.getResource(this.id).subscribe(response => {
      this.entity = response;
    })
  }

  save() {
    this.service.entityName = 'empresa';
    this.service.save(this.entity).subscribe(
      response => {
        this.router.navigate(['gerencial/empresas']);
        this.global.iziToas.success({ title: 'Dados salvos com sucesso' });
      },
      error => {
        this.global.iziToas.error({ title: 'Ocorreu um problema ao processar sua solicitação' });
      })
  }

  loadImage(image) {
    let tmpFile = image.target.files.item(0)
    this.uploadImage(tmpFile);

    if (FileReader) {
      var fr = new FileReader();
      fr.onload = () => {
        this.img = fr.result;
      }

      fr.readAsDataURL(tmpFile);
    }

    else {
      this.global.iziToas.warning({ title: 'Metodo não compátivel com sua versão' });
    }
  }

  getMarcas() {
    this.service.entityName = 'gerencial/marcas';
    this.service.getResources().subscribe(response => {
      this.marcas = response;
    })
  }

  uploadImage(file) {
    const formData = new FormData();
    formData.append('image', file);
    file.inProgress = true;
    this.service.upload(formData).pipe(
      map(event => {
        switch (event.type) {
          case HttpEventType.UploadProgress:
            file.progress = Math.round(event.loaded * 100 / event.total);
            console.log('file', file);
            break;
          case HttpEventType.Response:
            console.log('event', event);
            return event;
        }
      }),
      catchError((error: HttpErrorResponse) => {
        file.inProgress = false;
        return of(`${file.name} upload failed.`);
      })).subscribe((event: any) => {
        if (typeof (event) === 'object') {
          this.entity.logo = event.body.fileName;
        }
      });
  }

  generateIdentifier(text) {
    text = text.replace(/[^\w\s]/gi, '');
    text = text.split('  ').join(' ');
    text = text.split('-').join('');
    text = text.split(' ').join('-');
    text = text.split('--').join('-');
    text = text.toLowerCase();
    console.log(text);
    this.entity.identificador = text.split(' ').join('-');
  }

  getCep(cep:String) {
    console.log(cep.length);
    if (cep && cep.length >= 8) {
      this.blockUI.start('Buscando CEP');
      this.service.getCep(cep).subscribe(
        (response:any) => {
        this.blockUI.stop();
        this.entity.estado = response.uf;
        this.entity.cidade = response.localidade;
        this.entity.rua = response.logradouro;
        this.entity.numero = response.complemento;
        },
        error => {
          this.blockUI.stop();
          this.global.iziToas.error({
            title:'Ops',
            message:'Ocorreu um problema ao buscar seu cep, verifique e tente novamente.'
          })
        });
    }
  }

}
