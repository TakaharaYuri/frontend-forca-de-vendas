import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EmpresaComponent } from './empresa/empresa.component';
import { SetorComponent } from './setor/setor.component';
import { UsuarioComponent } from './usuario/usuario.component';
import { PerfilComponent } from './usuario/perfil/perfil.component';
import { ConfiguracoesComponent } from './configuracoes/configuracoes.component';
import { GerencialComponent } from './gerencial.component';
import { UsuarioDetalhesComponent } from './usuario/usuario-detalhes/usuario-detalhes.component';
import { EmpresaDetalhesComponent } from './empresa/empresa-detalhes/empresa-detalhes.component';
import { QrcodesComponent } from './qrcodes/qrcodes.component';
import { QrcodeDashboardComponent } from './qrcodes/qrcode-dashboard/qrcode-dashboard.component';
import { IntegracoesComponent } from './integracoes/integracoes.component';


const routes: Routes = [
  { path: '', component: GerencialComponent },

  { path: 'empresas', component: EmpresaComponent },
  { path: 'empresas/visualizar/:id', component: EmpresaDetalhesComponent },
  { path: 'empresas/novo', component: EmpresaDetalhesComponent },
  { path: 'setores', component: SetorComponent },
  { path: 'usuarios', component: UsuarioComponent },
  { path: 'usuarios/visualizar/:id', component: UsuarioDetalhesComponent },
  { path: 'usuarios/novo', component: UsuarioDetalhesComponent },
  { path: 'perfil', component: PerfilComponent },
  { path: 'configuracoes', component: ConfiguracoesComponent },
  { path: 'qrcodes', component: QrcodesComponent },
  { path: 'qrcodes/dashboard/:id', component: QrcodeDashboardComponent },
  { path: 'integracoes', component: IntegracoesComponent },
  { path: 'integracoes/syonet', component: IntegracoesComponent }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]   
})
export class GerencialRoutingModule { }


