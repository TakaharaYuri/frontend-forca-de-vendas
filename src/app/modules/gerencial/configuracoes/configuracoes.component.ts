import { Component, OnInit } from '@angular/core';
import { Global } from 'src/app/@core/global';
import { EvoService } from 'src/app/@core/evo.service';

@Component({
  selector: 'app-configuracoes',
  templateUrl: './configuracoes.component.html',
  styleUrls: ['./configuracoes.component.css']
})
export class ConfiguracoesComponent implements OnInit {

  public entity:any = {};

  constructor(
    private global: Global,
    private service: EvoService
  ) {
    this.service.entityName = 'configuracao';
   }

  ngOnInit(): void {
    this.get();
  }

  get() {
    this.service.getResources().subscribe( response => {
      this.entity = response;
    })
  }

  save() {
    if (this.entity.id) {
      this.service.updateResource(this.entity).subscribe( response => {
        this.global.iziToas.success({title:'Dados salvos com Sucesso'});
      })
    }
    else {
      this.service.createResource(this.entity).subscribe( response => {
        this.global.iziToas.success({title:'Dados salvos com Sucesso'});
      })
    }
  }

}
