import { Component, OnInit } from '@angular/core';
import { Global } from 'src/app/@core/global';
import { EvoService } from 'src/app/@core/evo.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalQrcodeComponent } from './modal-qrcode/modal-qrcode.component';

@Component({
  selector: 'app-qrcodes',
  templateUrl: './qrcodes.component.html',
  styleUrls: ['./qrcodes.component.css']
})
export class QrcodesComponent implements OnInit {

  public data;
  constructor(
    public global: Global,
    public service: EvoService,
    private modalService: NgbModal
  ) {

  }

  ngOnInit(): void {
    this.get();
  }

  get() {
    this.service.entityName = 'qrcodes';
    this.service.getResources().subscribe( response => {
      this.data = response;
    })
  }

  delete(id) {
    this.service.onDeleteConfirm(id).then(confirm => {
      if (confirm) {
        confirm.subscribe( response => {
          this.global.iziToas.success({title:'Registro removido com sucesso'});
          this.get();
        })
      }
    })
  }

  open(entity = {}) {
    const modal = this.modalService.open(ModalQrcodeComponent, {
      size: 'sm',
      keyboard: false,
      backdrop: 'static'
    });

    modal.componentInstance.entity = entity;

    modal.result.then((result) => {
      if (result) {
        this.get();
      }
    });
  }

}
