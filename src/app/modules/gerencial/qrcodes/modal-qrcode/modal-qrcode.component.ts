import { Component, OnInit, Input } from '@angular/core';
import { Global } from 'src/app/@core/global';
import { EvoService } from 'src/app/@core/evo.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-modal-qrcode',
  templateUrl: './modal-qrcode.component.html',
  styleUrls: ['./modal-qrcode.component.css']
})
export class ModalQrcodeComponent implements OnInit {

  @Input() entity: any = {};
  loading = false;
  resultUrlValidade;

  constructor(
    public global: Global,
    public service: EvoService,
    private activeModal: NgbActiveModal,
  ) { }

  ngOnInit(): void {
    if (!this.entity.tipo) {
      this.entity.tipo = 'interno';
    }
  }

  close() {
    this.activeModal.close(false);
  }

  save() {
    this.loading = true;
    this.service.entityName = 'qrcodes';
    this.service.save(this.entity).subscribe(response => {
      this.global.iziToas.success({ title: 'Formulario cadastrado com Sucesso' });
      this.activeModal.close(true);
    },
      error => {
        this.global.iziToas.error({ title: 'Ocorreu um problema ao processar sua requisição' });
      })
  }

  checkUrl(url) {
    this.loading = true;
    this.resultUrlValidade = null;
    this.service.entityName = `validar-url?url=${url}`;
    if (this.global.isValidUrl(url)) {
      this.service.getResources().subscribe(
        response => {
          this.loading = false;
          this.resultUrlValidade = 200;
        },
        error => {
          this.loading = false;
          this.resultUrlValidade = 400;
        })
    }
  }

}
