import { Component, OnInit } from '@angular/core';
import { Global } from 'src/app/@core/global';
import { EvoService } from 'src/app/@core/evo.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalCategoriasComponent } from './modal-categorias/modal-categorias.component';

@Component({
  selector: 'app-categorias',
  templateUrl: './categorias.component.html',
  styleUrls: ['./categorias.component.css']
})
export class CategoriasComponent implements OnInit {

  public data;

  constructor(
    public global: Global,
    public service: EvoService,
    private modalService: NgbModal
  ) { }

  ngOnInit(): void {
    this.service.entityName = 'estoque/categoria';
    this.get();
  }

  get() {
    this.service.getResources().subscribe(response => {
      this.data = response;
      console.log('Response ->', response);
    })
  }

  delete(id) {
    this.service.onDeleteConfirm(id, 'Os produtos ligados a esta categoria também serão removidos.').then(confirm => {
      if (confirm) {
        confirm.subscribe(response => {
          this.global.iziToas.success({ title: 'Registro removido com sucesso' });
          this.get();
        })
      }
    })
  }

  open(data = {}) {
    const modal = this.modalService.open(ModalCategoriasComponent, {
      size: 'sm',
      keyboard: false,
      backdrop: 'static'
    });

    modal.componentInstance.data = data;

    modal.result.then((result) => {
      if (result) {
        this.get();
      }
    });
  }

}
