import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { EvoService } from 'src/app/@core/evo.service';
import { Global } from 'src/app/@core/global';

@Component({
  selector: 'app-modal-categorias',
  templateUrl: './modal-categorias.component.html',
  styleUrls: ['./modal-categorias.component.css']
})
export class ModalCategoriasComponent implements OnInit {

  @Input() data:any = {};

  constructor(
    public global: Global,
    public service: EvoService,
    private activeModal: NgbActiveModal,
  ) { }

  ngOnInit(): void {
  }

  save() {
    this.service.save(this.data).subscribe( response => {
      this.global.iziToas.success({title:'Categoria cadastrado com Sucesso'});
      this.activeModal.close(true);
    },
    error => {
      this.global.iziToas.error({title:'Ocorreu um problema ao processar sua requisição'});
    })
  }

  close() {
    this.activeModal.close(false);
  }

}
