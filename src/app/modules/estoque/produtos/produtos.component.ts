import { Component, OnInit } from '@angular/core';
import { Global } from 'src/app/@core/global';
import { EvoService } from 'src/app/@core/evo.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalProdutosComponent } from './modal-produtos/modal-produtos.component';

@Component({
  selector: 'app-produtos',
  templateUrl: './produtos.component.html',
  styleUrls: ['./produtos.component.css']
})
export class ProdutosComponent implements OnInit {

  public data;

  constructor(
    public global: Global,
    public service: EvoService,
    private modalService: NgbModal
  ) { }

  ngOnInit(): void {
    this.get();
  }

  get() {
    this.service.entityName = 'estoque/produto';
    this.service.getResources().subscribe(response => {
      this.data = response;
      console.log('Response ->', response);
    })
  }

  delete(id) {
    this.service.onDeleteConfirm(id).then(confirm => {
      if (confirm) {
        confirm.subscribe(response => {
          console.log('DELETE', response);
          this.global.iziToas.success({ title: 'Registro removido com sucesso' });
          this.get();
        })
      }
    })
  }

  open(data = {}) {
    const modal = this.modalService.open(ModalProdutosComponent, {
      size: 'lg',
      keyboard: false,
      backdrop: 'static'
    });

    modal.componentInstance.entity = data;
    modal.result.then((result) => {
      if (result) {
        this.get();
      }
    });
  }

  updateStatus(item) {
    if (item.ativo == 1) item.ativo = 0;
    else if (item.ativo == 0) item.ativo = 1;
    this.service.updateResource(item).subscribe(response => {
      console.log('response =>', response);
      this.global.iziToas.success({ title: 'Status atualizado com sucesso' });
      this.get();
    })
  }

}
