import { Component, OnInit, Input } from '@angular/core';
import { Global } from 'src/app/@core/global';
import { EvoService } from 'src/app/@core/evo.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { HttpEventType, HttpErrorResponse } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { of } from 'rxjs';

@Component({
  selector: 'app-modal-produtos',
  templateUrl: './modal-produtos.component.html',
  styleUrls: ['./modal-produtos.component.scss']
})
export class ModalProdutosComponent implements OnInit {

  @Input() entity:any = {};
  img;
  categorias;

  constructor(
    public global: Global,
    public service: EvoService,
    private activeModal: NgbActiveModal,
  ) { }

  ngOnInit(): void {
    this.getCategorias();
  }

  getCategorias() {
    this.service.entityName = 'estoque/categoria';
    this.service.getResources().subscribe( response => {
      this.categorias = response;
    });
  }

  save() {
    this.service.entityName = 'estoque/produto';
    this.service.save(this.entity).subscribe( response => {
      this.global.iziToas.success({title:'Categoria cadastrado com Sucesso'});
      this.activeModal.close(true);
    },
    error => {
      this.global.iziToas.error({title:'Ocorreu um problema ao processar sua requisição'});
    })
  }

  close() {
    this.activeModal.close(false);
  }

  loadImage(image) {
    let tmpFile = image.target.files.item(0)
    this.uploadImage(tmpFile);

    if (FileReader) {
      var fr = new FileReader();
      fr.onload = () => {
        this.img = fr.result;
      }

      fr.readAsDataURL(tmpFile);
    }

    else {
      this.global.iziToas.warning({ title: 'Metodo não compátivel com sua versão' });
    }
  }

  uploadImage(file) {
    const formData = new FormData();
    formData.append('image', file);
    console.log('File', file);
    file.inProgress = true;
    this.service.upload(formData).pipe(
      map(event => {
        switch (event.type) {
          case HttpEventType.UploadProgress:
            file.progress = Math.round(event.loaded * 100 / event.total);
            console.log('file', file);
            break;
          case HttpEventType.Response:
            console.log('event', event);
            return event;
        }
      }),
      catchError((error: HttpErrorResponse) => {
        file.inProgress = false;
        return of(`${file.name} upload failed.`);
      })).subscribe((event: any) => {
        if (typeof (event) === 'object') {
          this.entity.imagem = event.body.fileName;
        }
      });
  }

}
