import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EstoqueRoutingModule } from './estoque-routing.module';
import { EstoqueComponent } from './estoque.component';
import { CategoriasComponent } from './categorias/categorias.component';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
import { ModalCategoriasComponent } from './categorias/modal-categorias/modal-categorias.component';
import { FormsModule } from '@angular/forms';
import { MomentModule } from 'ngx-moment';
import { ProdutosComponent } from './produtos/produtos.component';
import { ModalProdutosComponent } from './produtos/modal-produtos/modal-produtos.component';
import { NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxCurrencyModule } from "ngx-currency";
import { ComponentsModule } from 'src/app/components/components.module';



@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MomentModule,
    EstoqueRoutingModule,
    NgxSkeletonLoaderModule,
    NgbTooltipModule,
    NgxCurrencyModule,
    ComponentsModule
  ],
  declarations: [
    EstoqueComponent, 
    CategoriasComponent,
    ModalCategoriasComponent,
    ProdutosComponent,
    ModalProdutosComponent
  ]
})
export class EstoqueModule { }
