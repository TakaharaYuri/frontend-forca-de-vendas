import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { EvoService } from 'src/app/@core/evo.service';
import { Global } from 'src/app/@core/global';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-registro-cadastro',
  templateUrl: './registro-cadastro.component.html',
  styleUrls: ['./registro-cadastro.component.css']
})
export class RegistroCadastroComponent implements OnInit {
  public key;
  public empresaRef;
  public img;
  public entity: any = {
    cpf:null,
    celular:null
  };
  public loading = false;
  // public session;

  constructor(
    public global: Global,
    public service: EvoService,
    public route: ActivatedRoute,
    public router: Router
  ) {
    this.route.queryParams.subscribe(param => {
      if (param.key) {
        this.key = param.key;
        this.empresaRef = param.ref;
        let _session = JSON.parse(localStorage.getItem('newUser'));
        if (_session) {
          this.entity = _session.usuario;
          console.log(_session);
        }
        else {
          this.onValideError();
        }

      }
      else {
        this.onValideError();
      }
    });
  }

  ngOnInit(): void {
  }

  save() {
    this.loading = true;
    this.service.entityName = `convite/perfil`;
    this.service.createResource(this.entity).subscribe( 
      response => {
        // console.log('Response ->', response);
        this.router.navigate(['cadastro','perfil'], {queryParams:{ref:this.empresaRef, key:this.key}});
      },
      error => {
        // console.log('ResponseError ->', error);
      });
  }

  onValideError() {
    Swal.fire({
      title: 'Ops!',
      icon: 'warning',
      text: 'O link do seu convite não é válido. Solicite ao seu gerente um novo convite.',
      showCancelButton: true,
      confirmButtonText: 'Solicitar novo Convite',
      cancelButtonText: 'Fechar',
    }).then(result => {
      if (!result.value) {
        this.router.navigate(['login'])
      }
    })
  }

}
