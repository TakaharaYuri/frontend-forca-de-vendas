import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { EvoService } from 'src/app/@core/evo.service';
import { Global } from 'src/app/@core/global';

@Component({
  selector: 'app-gerar-imagem',
  templateUrl: './gerar-imagem.component.html',
  styleUrls: ['./gerar-imagem.component.css']
})
export class GerarImagemComponent implements OnInit {

  public loading = false;
  public entity: any = {};
  public data;
  public id;
  constructor(
    public service: EvoService,
    public global: Global,
    public activatedRoute: ActivatedRoute
  ) {
    this.service.entityName = "integracoes/gerador-imagens";
    this.activatedRoute.params.subscribe(param => {
      if (param.id) {
        this.id = param.id;
      }
    })

    if (localStorage.getItem('_entity')) {
      const _entity = JSON.parse(localStorage.getItem('_entity'))
      this.entity = _entity;
    }
  }

  ngOnInit(): void {
  }

  generate() {
    this.entity.id = this.id;
    this.loading = true;
    localStorage.setItem('_entity', JSON.stringify(this.entity));
    this.service.createResource(this.entity).subscribe(
      (response: any) => {
        this.data = response;
        this.loading = false;
      },
      (error: any) => {

      }
    )
  }


  reset() {
    this.loading = false;
    this.entity = {};
    this.data = null;
  }

  download() {
    let element =  document.getElementById("src_image");
    
  }

}
