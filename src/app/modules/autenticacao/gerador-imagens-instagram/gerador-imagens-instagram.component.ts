import { Component, OnInit } from '@angular/core';
import { EvoService } from 'src/app/@core/evo.service';

@Component({
  selector: 'app-gerador-imagens-instagram',
  templateUrl: './gerador-imagens-instagram.component.html',
  styleUrls: ['./gerador-imagens-instagram.component.css']
})
export class GeradorImagensInstagramComponent implements OnInit {
  public data;
  constructor(
    public service: EvoService
  ) {
    this.service.entityName = "integracoes/gerador-imagens";
  }

  ngOnInit(): void {
    this.get();
  }

  get() {
    this.service.getResources().subscribe(response => {
      this.data = response;
    })
  }

}
