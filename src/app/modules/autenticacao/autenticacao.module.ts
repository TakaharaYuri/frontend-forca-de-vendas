import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RegistroComponent } from './registro/registro.component';
import { RegistroCadastroComponent } from './registro/registro-cadastro/registro-cadastro.component';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';
import { RegistroPerfilComponent } from './registro/registro-perfil/registro-perfil.component';
import { YopsilonMaskModule } from 'yopsilon-mask';
import { GeradorImagensInstagramComponent } from './gerador-imagens-instagram/gerador-imagens-instagram.component';
import { GerarImagemComponent } from './gerador-imagens-instagram/gerar-imagem/gerar-imagem.component';
import { NgxMaskModule } from 'ngx-mask';
import { ComponentsModule } from 'src/app/components/components.module';



@NgModule({
    declarations: [RegistroComponent, RegistroCadastroComponent, RegistroPerfilComponent, GeradorImagensInstagramComponent, GerarImagemComponent],
    imports: [
        CommonModule,
        RouterModule,
        FormsModule,
        NgbTooltipModule,
        YopsilonMaskModule,
        NgxMaskModule.forRoot(),
        ComponentsModule
    ]
})
export class AutenticacaoModule {
    constructor() { }
}
